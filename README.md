# mongodb-gcs-backup

This project aims to provide a simple way to perform a MongoDB  backup using `mongodump` `Docker` `Kubernetes` and to upload it to Google Cloud Storage. It's a rewrite with NodeJS of [`takemetour/docker-mongodb-gcs-backup`](https://github.com/takemetour/docker-mongodb-gcs-backup).


### Docker image

You can pull the public image from Docker Hub:

    docker pull ebasley/kube-mongodb-gcs-backup:latest


## Setup GCS Bucket

see : https://cloud.google.com/storage/docs/creating-buckets


## Get credentials

Create a service account with a 'Storage Admin Role'

Get main.json credentials

see: https://cloud.google.com/iam/docs/creating-managing-service-account-keys and https://cloud.google.com/iam/docs/creating-managing-service-accounts


Use the resulting JSON key file within Kubernetes to create a secret from it by running the following command:

      kubectl create secret generic mongodb-gcs-backup --from-file=main.json=/path/to/your/key.json


## Dump a database

The following table lists the env variables you have to set up:

Environment Variable | Description
---------------------|-------------
`BUCKET_NAME`  | The bucket name where the `mongodump` archive files will be stored.
`GOOGLE_CLOUD_PROJECT` | Project ID
`GKE_KEY_FILE` |  Google credentials to access bucket

Create a CronJob in your kubernetes cluster like this:

```
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: mongo-gcs-backup
  namespace: prod
spec:
  schedule: "0 0 * * *"
  jobTemplate:
    spec:
      template:
         spec:
           containers:
           - name: mongo-gcs-backup
             image: ebasley/kube-mongodb-gcs-backup:latest
             env:
             - name: GKE_KEY_FILE
               value: "/opt/gcs/main.json"
             - name: GOOGLE_CLOUD_PROJECT
               value: "oecd-291807"
             - name: MONGO_URL
               value: "mongodb://mongo-prod-mongodb-replicaset-2.mongo-prod-mongodb-replicaset.prod.svc.cluster.local:27017/koopt"
             - name: BUCKET_NAME
               value: "backup"
             volumeMounts:
             - name: gcp-credentials
               mountPath: /opt/gcs
           restartPolicy: OnFailure
           volumes:
           - name: gcp-credentials
             secret:
               secretName: mongodb-gcs-backup
```


will run periodically:

```
$ mongodump --uri=[MONGO_URL] --archive --gzip
```

Archive files will be named [database]-[date].gz

Archives are purged at each execution with a yet hard coded retention (see `src/rotate.js`)


## Restore an archive

```
$ mongorestore --gzip --archive=koopt-2019-03-14T21_41_33.274Z.gz --uri='mongodb://0.0.0.0/koopt' --drop
```

