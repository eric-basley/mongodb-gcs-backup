const spawn = require('child_process').spawn;
const MongoURI = require('mongo-uri');

const createDumpArgs = ({ url }) => [`--uri=${url}`, `--archive`, '--gzip'];

module.exports = ctx =>
  new Promise(resolve => {
    console.log(ctx);
    const args = createDumpArgs(ctx);
    const { database = 'all' } = MongoURI.parse(ctx.url);
    console.log(`$ mongodump ${args.join(' ')}`);
    const mongodump = spawn('mongodump', args);
    mongodump.stderr.on('data', data => console.error(data.toString('ascii')));
    resolve({ ...ctx, database, dump: mongodump.stdout });
  });
