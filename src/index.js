const initConfig = require('./config');
const mongoDump = require('./mongodump');
const copyBucket = require('./bucket');
const rotateArchive = require('./rotate');

initConfig()
  .then(mongoDump)
  .then(copyBucket)
  .then(rotateArchive)
  .then(() => console.log('end of dump.'))
  .catch(console.error); // eslint-disable-line no-console
