const { getDateFromArchive, lastDays, lastOfDays, lastOfWeeks, lastOfMonths } = require('../utils');

const mockedDate = new Date(2020, 8, 1);
const OriDate = global.Date;
global.Date = jest.fn((...params) => (params.length ? new OriDate(...params) : mockedDate));

const d1 = new Date(2020, 8, 1);
const d2 = new Date(2020, 7, 31);
const d3 = new Date(2020, 7, 11);
const d4 = new Date(2020, 7, 14);

const data = [{}, { date: d2 }, { date: d1 }, { date: d3 }, { date: d4 }];

describe('utils', () => {
  it('should get date from archive', () => {
    const res = getDateFromArchive('koopt', { name: 'koopt-2020-09-01T00:00:14.805Z.gz' });
    expect(res.getFullYear()).toEqual(2020);
  });

  it('should not get date from archive', () => {
    const res = getDateFromArchive('koopt', { name: 'koopt-coucou.gz' });
    expect(res).toBeNull();
  });

  it('should get one last days', () => {
    const res = lastDays(0, data);
    expect(res.length).toEqual(1);
    expect(res[0].date).toEqual(d1);
  });

  it('should get two last days', () => {
    const res = lastDays(1, data);
    expect(res.length).toEqual(2);
    expect(res[1].date).toEqual(d1);
    expect(res[0].date).toEqual(d2);
  });

  it('should get last of days', () => {
    const res = lastOfDays(1, data);
    expect(res.length).toEqual(1);
    expect(res[0].date).toEqual(d1);
  });

  it('should get last of weeks', () => {
    const res = lastOfWeeks(3, data);
    expect(res.length).toEqual(2);
    expect(res[0].date).toEqual(d1);
    expect(res[1].date).toEqual(d4);
  });

  it('should get last of months', () => {
    const res = lastOfMonths(2, data);
    expect(res.length).toEqual(2);
    expect(res[0].date).toEqual(d1);
    expect(res[1].date).toEqual(d2);
  });
});
