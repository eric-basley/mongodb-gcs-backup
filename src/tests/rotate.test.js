const { bucketMaker } = require('@google-cloud/storage');
const { getArchives, select, purge } = require('../rotate');
const rotate = require('../rotate');
const { lastDays } = require('../utils');

jest.mock('@google-cloud/storage', () => {
  const bucketMaker = jest.fn();
  return {
    bucketMaker,
    Storage: class {
      bucket() {
        return bucketMaker();
      }
    },
  };
});

const mockedDate = new Date(2020, 8, 1);
const OriDate = global.Date;
global.Date = jest.fn(params => (params ? new OriDate(params) : mockedDate));

const files = [
  { name: 'koopt-2018-07-29T00:01:14.805Z.gz' },
  { name: 'koopt-2020-07-29T00:01:14.805Z.gz' },
  { name: 'koopt-2020-08-29T00:02:14.805Z.gz' },
  { name: 'koopt-2020-08-30T00:03:14.805Z.gz' },
  { name: 'koopt-2020-08-31T00:04:14.805Z.gz' },
  { name: 'koopt-2020-09-01T00:05:14.805Z.gz' },
  { name: 'toto-2020-09-01T00:06:14.805Z.gz' },
];

const database = 'koopt';

describe('rotate', () => {
  class Bucket {
    getFiles() {
      return Promise.resolve([files]);
    }
    file(name) {
      this.name = name;
      return this;
    }
    delete() {
      return Promise.resolve(this.name);
    }
  }
  it('should get archives', () => {
    const res = getArchives(database)(files);
    expect(res.length).toEqual(files.length - 1);
  });

  it('should select', () => {
    const archives = getArchives(database)(files);
    const res = select(lastDays(1))(archives);
    expect(res.length).toEqual(2);
    expect(res[1].length).toEqual(2);
  });

  it('should purge', done => {
    const deleted = jest.fn();

    const archives = getArchives(database)(files);
    const selected = select(lastDays(0))(archives);
    const purger = purge(new Bucket())(selected);
    purger.on('deleted', deleted);
    purger.on('done', ({ total, toBeDeleted }) => {
      expect(total.length).toEqual(6);
      expect(toBeDeleted.length).toEqual(5);
      expect(deleted).toHaveBeenCalledTimes(5);
      expect(deleted).toHaveBeenNthCalledWith(1, { name: 'koopt-2018-07-29T00:01:14.805Z.gz' });
      expect(deleted).toHaveBeenNthCalledWith(2, { name: 'koopt-2020-07-29T00:01:14.805Z.gz' });
      expect(deleted).toHaveBeenNthCalledWith(3, { name: 'koopt-2020-08-29T00:02:14.805Z.gz' });
      expect(deleted).toHaveBeenNthCalledWith(4, { name: 'koopt-2020-08-30T00:03:14.805Z.gz' });
      expect(deleted).toHaveBeenNthCalledWith(5, { name: 'koopt-2020-08-31T00:04:14.805Z.gz' });
      done();
    });
    purger();
  });

  it('should rotate', () => {
    bucketMaker.mockImplementation(() => new Bucket());
    rotate({ database: 'koopt' });
  });
});
