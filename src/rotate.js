const { Storage } = require('@google-cloud/storage');
const EventEmitter = require('events');
const { includes, reject, pluck, map, prop, filter, compose, reduce } = require('ramda');
const { getDateFromArchive, lastDays, lastOfDays, lastOfWeeks, lastOfMonths } = require('./utils');

const getArchives = database =>
  compose(
    map(archive => ({ name: archive.name, date: getDateFromArchive(database, archive) })),
    filter(file => new RegExp(`^${database}-`).test(file.name)),
  );

const select = (...fns) => archives => [archives, reduce((acc, fn) => [...acc, ...fn(archives)], [], fns)];

const purge = bucket => ([archives, toKeep]) => {
  const em = new EventEmitter();
  const deleteArchives = map(name =>
    bucket
      .file(name)
      .delete()
      .then(() => em.emit('deleted', { name })),
  );
  const namesToKeep = pluck('name', toKeep);
  const toBeDeleted = compose(
    reject(name => includes(name, namesToKeep)),
    pluck('name'),
  )(archives);
  const run = () =>
    Promise.all(deleteArchives(toBeDeleted)).then(() => em.emit('done', { total: archives, toBeDeleted }));
  run.on = (...params) => em.on(...params);
  return run;
};

module.exports = ctx => {
  const { bucketName, database } = ctx;
  const storage = new Storage();
  const bucket = storage.bucket(bucketName);
  return bucket
    .getFiles()
    .then(prop(0))
    .then(getArchives(database))
    .then(select(lastDays(0), lastOfDays(5), lastOfWeeks(5), lastOfMonths(4)))
    .then(purge(bucket))
    .then(purger => {
      const promise = new Promise(resolve => {
        purger.on('deleted', ({ name }) => console.log(`archive '${name}' deleted`)); // eslint-disable-line no-console
        purger.on('done', ({ total, toBeDeleted }) => {
          console.log(`${toBeDeleted.length}/${total.length} files to be purged...`); // eslint-disable-line no-console
          resolve(ctx);
        });
        purger();
      });
      return promise;
    });
};

module.exports.getArchives = getArchives;
module.exports.select = select;
module.exports.purge = purge;
