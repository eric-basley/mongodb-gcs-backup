const { Storage } = require('@google-cloud/storage');
const { getArchiveName } = require('./utils');

module.exports = ctx =>
  new Promise((resolve, reject) => {
    const { projectId, keyFilename, database, bucketName, dump } = ctx;
    const options = {};
    if (projectId) options.projectId = projectId;
    if (keyFilename) options.keyFilename = keyFilename;
    const storage = new Storage(options);
    const bucket = storage.bucket(bucketName);
    const fileName = getArchiveName(database);
    const file = bucket.file(fileName);
    dump
      .pipe(file.createWriteStream())
      .on('error', reject)
      .on('finish', () => {
        console.log(`'${fileName}' added to bucket '${bucketName}'`);
        resolve(ctx);
      });
  });
