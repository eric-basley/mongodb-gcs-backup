const { curry, sort, slice, values, prop, filter, compose, reduce } = require('ramda');
const { format, startOfDay, subDays } = require('date-fns');

const maxDate = (e1, e2) => (e1 ? (e1.date > e2.date ? e1 : e2) : e2);

const lastDays = curry((count, data) => {
  const refDate = startOfDay(subDays(new Date(), count));
  return filter(({ date }) => date >= refDate, data);
});

const lastOf = curry((str, count, data) =>
  compose(
    slice(0, count),
    sort((a, b) => b.date - a.date),
    values,
    reduce((acc, elt) => {
      if (!elt.date) return acc;
      const key = format(elt.date, str);
      acc[key] = maxDate(acc[key], elt);
      return acc;
    }, {}),
  )(data),
);

const lastOfDays = lastOf('yyyyMMdd');
const lastOfWeeks = lastOf('yyyyw');
const lastOfMonths = lastOf('yyyyMM');

const getArchiveName = (database = 'all') => `${database}-${new Date().toJSON()}.gz`;

const getDateFromArchive = (database = 'all', { name }) => {
  const date = new Date(prop(1, new RegExp(`^${database}-(.*)\.gz`).exec(name))); // eslint-disable-line no-useless-escape
  return isNaN(date) ? null : date;
};

module.exports = {
  getArchiveName,
  getDateFromArchive,
  lastOf,
  lastDays,
  lastOfDays,
  lastOfWeeks,
  lastOfMonths,
};
