const config = {
  keyFilename: process.env.GKE_KEY_FILE,
  projectId: process.env.GOOGLE_CLOUD_PROJECT,
  url: process.env.MONGO_URL || 'mongodb://0.0.0.0/koopt',
  bucketName: process.env.BUCKET_NAME || 'rp-main-backup',
};

module.exports = () => Promise.resolve(config);
