FROM node:16-alpine

RUN apk add --update mongodb-tools curl && rm -rf /var/cache/apk/*

RUN mkdir -p /opt
WORKDIR /opt

COPY .eslint* package.json yarn.lock  /opt/
COPY src /opt/src
COPY node_modules /opt/node_modules

EXPOSE 80
CMD yarn start
